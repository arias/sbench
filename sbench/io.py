#!/usr/bin/env python3

"""Definition of class IO."""

import sys


class IO:  # pylint: disable=R0903
    """Class IO provides some methods to print stuff."""

    VERBOSE: bool = False

    @classmethod
    def msg(cls, msg: str) -> None:
        """Print msg to stdout if IO.VERBOSE is True."""
        if IO.VERBOSE:
            print(msg)

    @classmethod
    def err(cls, msg: str) -> None:
        """Print an error message to stderr and exit with code 1."""
        print(f'error: {msg}', file=sys.stderr)
        sys.exit(1)
