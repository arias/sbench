#!/usr/bin/env python3

"""Definition of class Param."""

from typing import Dict

from . import Naming


class Param:
    """A Param object corresponds to a benchmark parameter."""

    def __init__(
            self,
            name: str,
            values: Dict[str, str]
    ):
        """Initialise self with a name and a possible values.

        The values dictionary maps value-names to arguments (possibly
        the empty string) that will passed to the command.

        >>> Param('reverse', {'yes': 'r', 'no': ''})
        <sbench.param.Param object at ...>

        """
        Naming.check(name, Naming.re_param_name)
        for value in values:
            Naming.check(value, Naming.re_param_value)
        self.__name = name
        self.__values = values

    @property
    def name(self) -> str:
        """Get the name of the parameter."""
        return self.__name

    @property
    def values(self) -> Dict[str, str]:
        """Get the possible values of the parameter."""
        return self.__values

    def get_value_arg(self, value: str) -> str:
        """Get the command line argument associated to value."""
        return self.__values[value]
