#!/usr/bin/env python3

"""Definition of class Filter."""

from __future__ import annotations
import typing as tp
import operator
from ply import lex, yacc

from .errors import FilterParseException
from . import Param, Run


class Filter:
    """Filters are boolean expression over a Benchmark parameters.

    Runs of the benchmark that do not satisfy its filters are ignored.

    """

    def __init__(self, expr: str):
        """Initialise self from expr (string of a filter expression).

        Raise FilterParseException if the expression could not be
        parsed.

        """
        self.__expr = expr
        self.__parsed = Filter.parse(expr)

    @property
    def expr(self) -> str:
        """Get the expression string of the filter."""
        return self.__expr

    def evaluate(
            self,
            assign: tp.Dict[Param, str],
            run: tp.Optional[Run] = None
    ) -> tp.Optional[bool]:
        """Evaluate filter with parameter assignment assign.

        The optional run which may be used to check for, e.g.,
        exit_code or timeout.

        Return True or False if the expression could have been
        evaluated with the given assignment.  Return None if the
        filter expression could not have been evaluated because it
        contains some parameter x for which x is not in assign.

        """
        #  pylint: disable=comparison-with-callable
        def evaluate_param(expr: tp.Sequence[tp.Any]) -> tp.Any:
            x = expr[0]
            if x in ['exit_code', 'timeout']:
                if run is not None and x in run.result:
                    return run.result[
                        tp.cast(tp.Literal['exit_code', 'timeout'], x)
                    ]
                return None
            p = next((p for p in assign if p.name == x), None)
            if p is None:
                return None
            return assign[p]

        def evaluate_constant(expr: tp.Sequence[tp.Any]) -> tp.Any:
            return expr[0]

        def evaluate_op(expr: tp.Sequence[tp.Any]) -> tp.Any:
            subs = list(map(rec, expr[1:]))
            if subs.count(None) == 0:
                return expr[0](*subs)
            return None

        def evaluate_op_lazy(expr: tp.Sequence[tp.Any]) -> tp.Any:
            op = expr[0]
            eval_ok = True
            subs = list(map(rec, expr[1:]))
            for sub in subs:
                eval_ok = eval_ok and sub is not None
                if sub is not None:
                    if op == operator.and_ and not sub:
                        return False
                    if op == operator.or_ and sub:
                        return True
            if eval_ok:
                return {
                    operator.or_: False,
                    operator.and_: True
                }[op]
            return None

        def rec(expr: tp.Sequence[tp.Any]) -> tp.Any:
            return {
                'BINOP': evaluate_op_lazy,
                'PARAM': evaluate_param,
                'CONST': evaluate_constant,
                'PRED': evaluate_op,
                'UNOP': evaluate_op
            }[expr[0]](expr[1:])
        return tp.cast(tp.Optional[bool], rec(self.__parsed))

    @classmethod
    def parse(cls, expr: str) -> tp.Sequence[tp.Any]:
        """Parse filter expr and return a Tuple encoding this filter.

        >>> Filter.parse('x == 3')
        ('PRED', <built-in function eq>, ('PARAM', 'x'), ('CONST', 3))
        >>> Filter.parse("x == 'test'")
        ('PRED', <built-in function eq>, ('PARAM', 'x'), ('CONST', 'test'))
        >>> Filter.parse('exit_code != 0 || x == 2')
        ('BINOP', <built-in function or_>, ('PRED', <built-in function ne>, \
('PARAM', 'exit_code'), ('CONST', 0)), ('PRED', <built-in function eq>, \
('PARAM', 'x'), ('CONST', 2)))

        """
        # pylint: disable=unused-variable
        # pylint: disable=too-many-locals

        tokens = [
            'lparen',
            'rparen',
            'param',
            'string',
            'equal',
            'equiv',
            'imply',
            'diff',
            'and',
            'not',
            'or',
            'int'
        ]
        t_lparen = r'\('
        t_rparen = r'\)'
        t_param = r'[a-zA-Z][a-zA-Z\-\_0-9]*'
        t_string = r'\'[a-zA-Z\-\_0-9\.]+\''
        t_int = r'[0-9]+'
        t_equal = '=='
        t_imply = '=>'
        t_diff = '!='
        t_and = '&&'
        t_or = r'\|\|'
        t_not = '!'
        t_ignore = ' \t'

        precedence = (
            ('left', 'or'),
            ('left', 'and'),
            ('left', 'imply'),
            ('left', 'not')
        )

        un_op = {
            t_not: operator.not_
        }
        bin_op = {
            t_and: operator.and_,
            '||': operator.or_,
            '!': operator.not_,
            t_equal: operator.eq,
            t_diff: operator.ne
        }

        def p_expr_paren(p: tp.Any) -> None:  # noqa: D400,D403,D205,D208
            """expr : lparen expr rparen"""
            p[0] = p[2]

        def p_expr_bin_op(p: tp.Any) -> None:  # noqa: D400,D403,D205,D208
            """expr : expr and expr
                    | expr or expr
                    | expr imply expr
            """
            if p[2] == t_imply:
                p[0] = (
                    'BINOP',
                    operator.or_,
                    ('UNOP', operator.not_, p[1]),
                    p[3]
                )
            else:
                p[0] = ('BINOP', bin_op[p[2]], p[1], p[3])

        def p_param(p: tp.Any) -> None:  # noqa: D400,D401,D403,D205,D208
            """_param : param"""
            p[0] = ('PARAM', p[1])

        def p_string(p: tp.Any) -> None:  # noqa: D400,D401,D403,D205,D208
            """_string : string"""
            p[0] = ('CONST', p[1][1:-1])  # remove quotes

        def p_int(p: tp.Any) -> None:  # noqa: D400,D401,D403,D205,D208
            """_int : int"""
            p[0] = ('CONST', int(p[1]))

        def p_par_or_cst(p: tp.Any) -> None:  # noqa: D400,D401,D403,D205,D208
            """param_or_const : _param
                              | _string
                              | _int
            """
            p[0] = p[1]

        def p_expr_pred(p: tp.Any) -> None:  # noqa: D400,D401,D403,D205,D208
            """expr : param_or_const equal param_or_const
                    | param_or_const diff  param_or_const
            """
            p[0] = ('PRED', bin_op[p[2]], p[1], p[3])

        def p_expr_un_op(p: tp.Any) -> None:  # noqa: D400,D401,D403,D205,D208
            """expr : not expr"""
            p[0] = ('UNOP', un_op[p[1]], p[2])

        def t_error(t: tp.Any) -> None:
            raise FilterParseException(expr)

        def p_error(p: tp.Any) -> None:
            raise FilterParseException(expr)

        lex.lex()
        return tp.cast(tp.Sequence[tp.Any], yacc.yacc().parse(expr))
