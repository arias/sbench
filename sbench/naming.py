#!/usr/bin/env python3

"""Definition of class Naming."""

import re

from .errors import NamingException


class Naming:  # pylint: disable=too-few-public-methods
    """Class Naming defines some regular expressions for naming."""

    #  a benchmark name
    re_benchmark_name = r'[a-zA-Z][\.a-zA-Z\-\_0-9]*'

    #  a parameter name
    re_param_name = r'[a-zA-Z][\.a-zA-Z\-\_0-9]*'

    #  a parameter value
    re_param_value = r'[\.a-zA-Z\-\_0-9]+'

    @classmethod
    def check(cls, name: str, name_re: str) -> None:
        """Check that name matches name_re regular expression.

        Raise Errors.NamingException if the expression is not matched.

        """
        if not re.compile(name_re).fullmatch(name):
            raise NamingException(str(name), str(name_re))
