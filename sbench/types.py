#!/usr/bin/env python3

"""Definition of some types for type checking."""

import typing as tp

benchmark_stats_t = tp.TypedDict('benchmark_stats_t', {
    'name': str,
    'done': int,
    'not_done': int,
    'success': int,
    'failure': int,
    'timeout': int
})

run_result_t = tp.TypedDict('run_result_t', {
    'command': str,
    'assign': tp.Dict[str, str],
    'start_time': str,
    'end_time': str,
    'exit_code': tp.Optional[int],
    'exec_time': float,
    'timeout': bool
}, total=False)
