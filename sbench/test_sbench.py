#!/usr/bin/env python3

"""Test module for sbench."""

import tempfile
from . import Benchmark


def mk_test_ls_benchmark() -> Benchmark:
    """Return a test benchmark that execute ls with various parameters."""
    return Benchmark(**{
        "name": "test-ls",
        "cmd": "/usr/bin/ls -1",
        "timeout": 2,
        "params": {
            "long": {
                "long": "-l",
                "no-long": ""
            },
            "reverse": {
                "reverse": "-r",
                "no-reverse": ""
            },
            "all": {
                "all": "-a",
                "no-all": ""
            },
            "recursive": {
                "recursive": "-R",
                "no-recursive": ""
            },
            "dir": {
                "tmp": "/tmp",
                "root": "/root",
                "usrbin": "/usr/bin",
                "all": "/"
            }
        },
        "filters": [
            "recursive == 'recursive' => dir != 'tmp'"
        ]
    })


def test() -> None:
    """Test function for sbench.

    Create a test benchmark, launch it and do various stuff on it.

    """
    benchmark = mk_test_ls_benchmark()
    benchmark.run()
    for row in benchmark.extract():
        print(row)
    print(benchmark.stats())
    benchmark.show()
    zip_file = tempfile.mktemp(suffix='.zip')
    benchmark.archive(zip_file)
    benchmark.unarchive(zip_file)
    benchmark.delete()
