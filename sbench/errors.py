#!/usr/bin/env python3

"""Definition of some Exception classes."""


class FilterParseException(Exception):
    """Exception raised when a filter could not be parsed."""

    def __init__(self, filter_expr: str):
        """Initialise self.

        filter_expr = filter expression that could not be parsed.

        """
        super().__init__(self)
        self.__filter_expr = filter_expr

    @property
    def filter_expr(self) -> str:
        """Get the filter expression."""
        return self.__filter_expr


class NamingException(Exception):
    """Exception raised when a name does not respect a naming convention."""

    def __init__(self, name: str, name_re: str):
        """Initialise self.

        name = provided name, name_re = regular expression the name
        should have matched.

        """
        super().__init__(self)
        self.__name = name
        self.__name_re = name_re

    def __str__(self) -> str:
        return f'"{self.name}" does not match "{self.name_re}"'

    @property
    def name(self) -> str:
        """Get the provided name."""
        return self.__name

    @property
    def name_re(self) -> str:
        """Get the regular expression."""
        return self.__name_re
