#!/usr/bin/env python3

"""Provide get_argparser that returns sbench's argument parser."""

import argparse

from . import mdata


def get_argparser() -> argparse.ArgumentParser:
    """Return sbench's argument parser."""
    result = argparse.ArgumentParser(
        description='simple benchmarking tool'
    )
    result.add_argument(
        '-a', '--action', default='run', type=str,
        choices=['run', 'stats', 'extract', 'show', 'delete', 'zip', 'unzip'],
        help='set action to perform'
    )
    result.add_argument(
        '-z', '--zip-file', type=str,
        help='(with options `-a zip` or `-a unzip`) output zip file'
    )
    result.add_argument(
        '-n', '--handler', type=str,
        help=(
            '(with option `-a extract`) set the command that will be used ' +
            'to extract each test to CSV data'
        )
    )
    result.add_argument(
        '-r', '--random', action='store_true',
        help='(with option `-a run`) randomize tests'
    )
    result.add_argument(
        '-v', '--verbose', action='store_true',
        help='be verbose'
    )
    result.add_argument(
        '-V', '--version', action='version',
        version=f'%(prog)s {mdata.VERSION}',
        help='print the version number and exit'
    )
    result.add_argument(
        '-f', '--filter', type=str, action='append',
        help='add a filter expression'
    )
    result.add_argument(
        '-t', '--rerun-timeouts', action='store_true',
        help=(
            '(with option `-a run`) rerun tests that ended with a timeout ' +
            'during a previous execution of sbench'
        )
    )
    result.add_argument(
        '-e', '--rerun-errors', action='store_true',
        help=(
            '(with option `-a run`) rerun tests that ended with an error ' +
            'during a previous execution of sbench'
        )
    )
    result.add_argument(
        '--force', action='store_true',
        help=(
            '(with option `-a delete`) do not prompt the user and force ' +
            'the deletion of test data'
        )
    )
    result.add_argument(
        'json', type=str,
        help='json file of the benchmark to process'
    )
    return result
