#!/usr/bin/env python3

"""Definition of class Run."""

from __future__ import annotations
import typing as tp
from typing import TYPE_CHECKING
import os
import subprocess
import json
import datetime
from pathlib import Path
import shutil
import hashlib

from . import Param, IO, types
if TYPE_CHECKING:
    from . import Benchmark


class Run:  # pylint: disable=R0902,R0904
    """A Run is an instanciation of the parameters of a Benchmark."""

    DATA_FILE = '_data.json'
    STDERR_FILE = '_stderr'
    STDOUT_FILE = '_stdout'

    def __init__(
            self,
            benchmark: 'Benchmark',
            assign: tp.Dict[Param, str]
    ):
        """Initialise self with the given benchmark and a parameter assignment.

        Dictionary assign maps Parameters to couple (name, arg) where
        name is the name associated to the value and arg is the
        argument that will be passed to the command.

        """
        self.__benchmark = benchmark
        self.__assign = assign
        self.__result: types.run_result_t = dict()
        self.__done = False

        #  read the results from the json file of the run
        try:
            with open(self.get_data_file(), 'r', encoding='utf-8') as fd:
                self.__result = json.loads(fd.read())
                self.__done = (
                    os.path.exists(self.get_stderr())
                    and os.path.exists(self.get_data_file())
                )
        except (json.decoder.JSONDecodeError, FileNotFoundError):
            pass

    @property
    def benchmark(self) -> 'Benchmark':
        """Get the benchmark of the run."""
        return self.__benchmark

    @property
    def assign(self) -> tp.Dict[Param, str]:
        """Get the parameter assignment of the run."""
        return self.__assign

    @property
    def result(self) -> types.run_result_t:
        """Get the result of the run as a dictionary.

        The dictionary contains the following keys:
        * command
        * assign
        * start_time
        * end_time
        * exit_code
        * exec_time
        * timeout

        The resulting dictionary is empty if the run has not been
        launched yet.

        """
        return self.__result

    def get_full_cmd(self) -> str:
        """Get the full command that will be launched for the run.

        The full command is obtained by concatenating the base command
        with parameter values.

        """
        result = self.benchmark.cmd
        for param, value in self.assign.items():
            arg = param.get_value_arg(value)
            if arg != '':
                result += ' ' + arg
        return result

    def is_already_done(self) -> bool:
        """Check if the run has completed (i.e., all related files exists)."""
        return self.__done

    def is_success(self) -> bool:
        """Check if self has successfully completed.

        Return True if it has been launched and has terminated with an
        exit code != 0, False otherwise.

        """
        return self.__result.get('exit_code') == 0

    def is_timeout(self) -> bool:
        """Check if the run did timeout.

        Return True if the run has been launched and has not
        terminated within the benchmark timeout value.

        """
        return self.__result.get('timeout', False)

    def get_data_file(self) -> str:
        """Get the data file of the run.

        The data file stores, after completion of the run, various
        data on how the run performed (i.e., exit code, timeout).

        """
        return os.path.join(self.get_dir(), Run.DATA_FILE)

    def get_stdout(self) -> str:
        """Get the file containing run's stdout after completion."""
        return os.path.join(self.get_dir(), Run.STDOUT_FILE)

    def get_stderr(self) -> str:
        """Get the file containing run's stderr after completion."""
        return os.path.join(self.get_dir(), Run.STDERR_FILE)

    def get_data_dir(self) -> str:
        """Get the directory in which the run may store specific data."""
        return self.get_dir()

    def delete_dir(self) -> bool:
        """Delete the directory of the run."""
        run_dir = self.get_dir()
        if os.path.isdir(run_dir):
            shutil.rmtree(run_dir)
            IO.msg(f'> delete {run_dir}')
            return True
        return False

    def get_dir(self, prefix_with_bench_dir: bool = True) -> str:
        """Get the directory of the run (each run has a unique directory)."""
        sub_dirs = list(self.assign.values())
        hval = hashlib.sha256(';'.join(sub_dirs).encode('utf-8')).hexdigest()
        result = os.path.join(hval[:2], hval[2:4], hval[4:])
        # result = os.path.join(*sub_dirs)
        if prefix_with_bench_dir:
            result = os.path.join(self.benchmark.get_runs_dir(), result)
        return result

    def env_vars(self) -> tp.Dict[str, str]:
        """Get the environment variables that are set before executing the run.

        Environment variables are:
        * SBENCH_DATA_DIR
        * SBENCH_STDOUT
        * SBENCH_STDERR

        """
        return {
            **os.environ,
            'SBENCH_DATA_DIR': self.get_data_dir(),
            'SBENCH_STDOUT': self.get_stdout(),
            'SBENCH_STDERR': self.get_stderr()
        }

    def launch(self) -> None:
        """Launch the run by executing self.get_full_cmd()."""
        full_cmd = self.get_full_cmd()
        IO.msg(full_cmd)
        out_dir = self.get_dir()
        Path(out_dir).mkdir(parents=True, exist_ok=True)
        data_dir = self.get_data_dir()
        Path(data_dir).mkdir(parents=True, exist_ok=True)
        start_time = datetime.datetime.now()
        with (
                open(self.get_stderr(), 'w', encoding='utf-8') as fd_err,
                open(self.get_stdout(), 'w', encoding='utf-8') as fd_out
        ):
            try:
                proc = subprocess.run(
                    full_cmd,
                    shell=True,
                    stdout=fd_out,
                    stderr=fd_err,
                    encoding='utf-8',
                    env=self.env_vars(),
                    timeout=self.benchmark.timeout,
                    check=False
                )
                timeout = False
                exit_code: tp.Optional[int] = proc.returncode
                IO.msg(f'> done (exit code = {exit_code})!')
            except subprocess.TimeoutExpired:
                timeout = True
                exit_code = None
                IO.msg('> timeout!')
        end_time = datetime.datetime.now()
        diff = end_time - start_time
        exec_time = diff.seconds + diff.microseconds / 1_000_000
        with open(self.get_data_file(), 'w', encoding='utf-8') as fd:
            data: types.run_result_t = {
                'command': self.get_full_cmd(),
                'assign': {
                    param.name: value
                    for param, value in self.assign.items()
                },
                'start_time': str(start_time),
                'end_time': str(end_time),
                'exit_code': exit_code,
                'exec_time': exec_time,
                'timeout': timeout
            }
            fd.write(json.dumps(data, indent=2))
            self.__result = data
        self.__done = True

    def extract_csv(
            self,
            cmd: tp.Optional[str] = None,
            fun: tp.Optional[tp.Callable[[Run], tp.Iterator[str]]] = None
    ) -> tp.Iterator[str]:
        """Generate a csv line for self containing run data.

        Values are separated with character ';'.  Does not generate
        anything if the run has not completed yet.

        If cmd is not None, then it must contain a command that will
        be executed for this run and that must print to standard
        output a list of ';' separated values that are appended to run
        data.  self.env_vars() are set before calling the command.

        Similarly, if fun is not None, then it must yield the strings
        that are appended to run data for the run.

        """
        if self.is_already_done():
            row = [
                *self.assign.values(),
                str(self.result['exit_code']),
                str(int(self.result['timeout'])),
                str(self.result['exec_time'])
            ]
            if cmd is not None:
                with subprocess.Popen(
                    cmd,
                    shell=True,
                    stdout=subprocess.PIPE,
                    encoding='utf-8',
                    env=self.env_vars()
                ) as proc:
                    if proc.stdout is not None:
                        loop = True
                        while loop:
                            line = proc.stdout.readline()
                            if line != '':
                                yield ';'.join(row) + ';' + line.split('\n')[0]
                            loop = line != '' or proc.poll() is None
            elif fun is not None:
                for line in fun(self):
                    yield ';'.join(row) + ';' + line
            else:
                yield ';'.join(row)

    def serialise(self) -> tp.Dict[str, tp.Any]:
        """Get a json dumpable dictionary containing all the run data."""
        result: tp.Dict[str, tp.Any] = {
            'command': self.get_full_cmd(),
            'exit_code': None,
            'exec_time': None,
            'timeout': None,
            'stdout': None,
            'stderr': None,
            'start_time': None,
            'end_time': None
        }
        if self.is_already_done():
            my_dir = self.get_dir(prefix_with_bench_dir=False)
            result = {
                **result,
                **self.result,
                'stdout': os.path.join('runs', my_dir, Run.STDOUT_FILE),
                'stderr': os.path.join('runs', my_dir, Run.STDERR_FILE)
            }
        result['assign'] = {p.name: v for p, v in self.assign.items()}
        return result

    def show(self) -> None:
        """Print information of the run to stdout.

        Informations printed include the contents of stdout, stderr,
        and data.json.

        """
        print('>>>>>  data  <<<<<')
        for key, val in self.result.items():
            print(key, '=', val)
        for name, path in [
                ('stdout', self.get_stdout()),
                ('stderr', self.get_stderr())
        ]:
            if os.path.isfile(path):
                print(f'>>>>> {name} <<<<<')
                with open(path, encoding='utf-8') as fd:
                    try:
                        print(fd.read())
                    except UnicodeDecodeError:
                        pass
