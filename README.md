# sbench


## description

sbench is a simple benchmarking tool written in python


## license

sbench is published under the term of
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)


## installation

install sbench with the `setup.py` script

```
python setup.py install --user
```


## usage

```
usage: sbench [-h] [-a {run,stats,extract,show,delete,zip,unzip}]
              [-z ZIP_FILE] [-n HANDLER] [-r] [-v] [-V] [-f FILTER] [-t] [-e]
              [--force]
              json

simple benchmarking tool

positional arguments:
  json                  json file of the benchmark to process

optional arguments:
  -h, --help            show this help message and exit
  -a {run,stats,extract,show,delete,zip,unzip}, --action {run,stats,extract,show,delete,zip,unzip}
                        set action to perform
  -z ZIP_FILE, --zip-file ZIP_FILE
                        (with options `-a zip` or `-a unzip`) output zip file
  -n HANDLER, --handler HANDLER
                        (with option `-a extract`) set the command that will
                        be used to extract each test to CSV data
  -r, --random          (with option `-a run`) randomize tests
  -v, --verbose         be verbose
  -V, --version         print the version number and exit
  -f FILTER, --filter FILTER
                        add a filter expression
  -t, --rerun-timeouts  (with option `-a run`) rerun tests that ended with a
                        timeout during a previous execution of sbench
  -e, --rerun-errors    (with option `-a run`) rerun tests that ended with an
                        error during a previous execution of sbench
  --force               (with option `-a delete`) do not prompt the user and
                        force the deletion of test data

```
